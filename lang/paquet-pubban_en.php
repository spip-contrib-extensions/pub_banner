<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pubban?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pubban_description' => 'Management and statistics for ad banners integrated in SPIP skeletons ...

{{USE}}

- * This plugin adds the {{ <code>#PUBBAN {banner name}</code> }} to the skeleton, which is replaced by a frame encompassing the banner.
- * A public page allows you to present the statistics of the inserts and exports them in ’.xls’ format.

{{DEFAULT VALUES}}

- * Standard banners are loaded by default during installation, they are the four most common types on the web. You can delete them and create new ones.
- * Examples of inserts ({SPIP squirrel}) are also loaded for demonstration purposes.

{{HELP / DOC}}

- * An explanation and a complete example are available in the documentation, you will also see the integration of banners in your skeletons. When the plugin is activated on your site this documentation is available on the public page "pubban_documentation."

- * The statistics page of the plugin is a free adaptation of the script ’{{[VRG pub->http://vrgpub.frankdevelopper.com/]}}’, developed by {{Vincent Roseberry}} ({GPL licensed at that time}).',
	'pubban_slogan' => ' Ads banners manager'
);
